#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QAudioInput>
#include <QTimer>
#include <QBuffer>
#include <snowboy-detect.h>

using snowboy::SnowboyDetect;

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void detect_hotword();

private:
    Ui::Widget *ui;
    QAudioInput* recorder;
    SnowboyDetect* detector;
    QTimer* timer;
    QBuffer* data;
};
#endif // WIDGET_H
