#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_connectButton_clicked();
    void on_sendButton_clicked();
    void onConnected();
    void onDisconnected();
    void onReceived();

private:
    Ui::Widget *ui;
    QTcpSocket* sock;
};

#endif // WIDGET_H
